package com.app.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.app.core.StringCalculator;

class CalculatorTest {

	@Test
	void testEmptyString() {
		int result = StringCalculator.Add(" ");
		assertTrue(result == 0);
	}
	
	@Test
	void testStringforTwoNumber() {
		int result = StringCalculator.Add("1,2");
		assertTrue(result == 3);
	}
	
	@Test
	void testForHandleUnknownAmountofNumbers() {
		assertEquals(4+5+9+12+25+33, StringCalculator.Add("4,5,9,12,25,33"));
		
	}
	
	@Test
	void testStringForNewLine() {
		int result = StringCalculator.Add("1\n,2");
		assertTrue(result == 3);
		
	}
	
	@Test
	void testStringForDifferentDelimiter() {
		int result = StringCalculator.Add("1,2, ,\n,3,1;7:8|7/6//6");
		assertTrue(result == 41);
		
	}
	
	@Test
	void testStringForNegativeNubmer() {
		int result = StringCalculator.Add("1,2\n,3,-5,7");
		assertTrue(result == 0);
		
	}
	
	@Test
	public final void IfOneOrMoreNumbersAreGreaterThan1000ThenItnotInSum() {
	    int result = StringCalculator.Add("5,1001,7,1278");
	    assertTrue(result == 12);
	}
	
	@Test
	void testStringDelimeterCanbeofanylengthDelimiter() {
		int result = StringCalculator.Add("[***]\n1***2***3","*[]\n");
		assertTrue(result == 6);
		
	}
	
	@Test
	void testStringMultipleDelimiter() {
		int result = StringCalculator.Add("//[*][%]\n1*2%3","/%*[]\n");
		assertTrue(result == 6);
		
	}
	
	@Test
	void testStringMultipleDelimiterLengthLongerThanOneChar() {
		int result = StringCalculator.Add("1delim2**3","delim*");
		assertTrue(result == 6);
		
	}
}
